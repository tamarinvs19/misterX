from random import randint
from itertools import product
import logging as log
from pprint import pprint

from django.views import generic
from django.views.generic.edit import FormView
from django.shortcuts import render, get_object_or_404
from django.http import Http404, JsonResponse

from django.utils import timezone

from .models import Game, GamersInGame
from .forms import CreateGameForm
from django.contrib.auth.models import User

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from . import generation
from . import validate_action
from . import steps
from . import translation_text

log.basicConfig(
        level='INFO',
        format='%(asctime)s %(name)s  %(levelname)s  %(filename)s  %(funcName)s  --> %(message)s',
        filename='./misterx.log',
        filemode='a',
        )


@method_decorator(login_required, name='dispatch')
class IndexView(generic.ListView): #test
    template_name = 'games/index.html'
    context_object_name = 'game_list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(translation_text.get_context_for_navbar())
        context.update(translation_text.get_context_for_index())
        return context

    def get_queryset(self):
        return Game.objects.filter(
                create_date__lte=timezone.now()).order_by('-create_date')


@method_decorator(login_required, name='dispatch')
class DetailView(generic.DetailView): #test
    model = Game
    template_name = 'games/detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(translation_text.get_context_for_navbar())
        context.update(translation_text.get_context_for_detail_game())
        return context

    def get_queryset(self):
        return Game.objects.filter(
                create_date__lte=timezone.now()).order_by('-create_date')


@method_decorator(login_required, name='dispatch')
class CreateGame(FormView): #test
    form_class = CreateGameForm
    success_url = "/games/"
    template_name = "games/createGame.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(translation_text.get_context_for_navbar())
        context.update(translation_text.get_context_for_create_game())
        return context

    def form_valid(self, form):
        form.save(form)
        return super(CreateGame, self).form_valid(form)


@login_required
def open_main(request, pk, **kwargs):
    game = Game.objects.get(id=pk)
    if game.start_setting_status == False:
        game.initialization()

    context = {
            'game': game
            }
    context.update(translation_text.get_context_for_main())

    return render(request, 'games/gameField.html', context)

@login_required
def end_game(request, pk, **kwargs):
    game = Game.objects.get(id=pk)
    win = {(True, False): 'misterx', (False, True): 'detectives', (True, True): 'detectives', (False, False): 'nobody'}[(validate_action.validate_win_misterx(game), validate_action.validate_win_detectives(game))]

    context = {
            'win': win,
            'city': game.get_cities['misterx']['white'],
            'game': game
            }
    context.update(translation_text.get_context_for_navbar())
    context.update(translation_text.get_context_for_end())

    return render(request, 'games/endGame.html', context)


@login_required
def setup_view(request, pk, **kwargs): #test
    template_name = "games/setupGamePage.html"
    game = get_object_or_404(Game, id=pk)
    if game.create_date <= timezone.now() <= game.finish_date:
        gamer = request.user
        game.add_gamer(gamer)

        ging = GamersInGame.objects.get(game=game, gamer=gamer)
        colors = ['green', 'yellow', 'orange']
        context={
            'game': game,
            'pk': pk,
            'options': ['None', 'I-am-ready'],
            'role': ging.role,
            'colors': [
                (color, translation_text.translate_color(color))
                for color in colors ],
            'color_misterx': ging.color_misterx,
            'players': [gamer.id for gamer in game.gamers.all()],
            }
        context.update(translation_text.get_context_for_navbar())
        context.update(translation_text.get_context_for_setup())

        return render(request,
                template_name=template_name,
                context=context,
                )
    else:
        raise Http404("Game does not exist")


@login_required
def status_gamers(request, pk, **kwargs):#+
    if request.method == 'GET':
        game = get_object_or_404(Game, pk=pk)

        usernames = [(lobby.gamer.id, lobby.gamer.username)
                for lobby in GamersInGame.objects.filter(game=pk)]
        data = {
                'players': [(
                    lobby.gamer.id, 
                    lobby.ready,
                    lobby.role,
                    lobby.gamer==request.user,
                    lobby.gamer.gender,
                    )
                    for lobby in GamersInGame.objects.filter(game=pk)],
                'max_count': game.max_count_players,
                'usernames': usernames,
                }
        log.info(data)
        return JsonResponse(data)

    elif request.method == 'POST':
        gamer = request.user
        game = get_object_or_404(Game, pk=pk)
        ging = GamersInGame.objects.get(game=game, gamer=gamer)
        if request.POST.get('status', None) == "I-am-ready":
            ging.ready = True
            ging.save()
        if request.POST.get('status', None) in {"Yes", "Да"}:
            ging.role = 'misterx'
            ging.save()
        data = {'success': True, 'role': ging.role, 'ready': ging.ready}
        log.info(data)
        return JsonResponse(data)
    raise Http404

@login_required
def color_misterx(request, pk, **kwargs):
    if request.method == 'POST':
        game = get_object_or_404(Game, pk=pk)
        gamer = request.user
        ging = GamersInGame.objects.get(game=game, gamer=gamer)
        color = translation_text.translate_color(request.POST.get('color', None))
        ging.color_misterx = color
        ging.save()

        data = {'success': True, 'color_misterx': ging.color_misterx}
        log.info(data)
        return JsonResponse(data)
    raise Http404

@login_required
def setting_roles(request, pk, **kwargs): # Perhaps, I can delete it. setting_games has the best analog
    if request.method == 'POST':
        game = get_object_or_404(Game, pk=pk)
        if game.count_gamers_misterx == 0:
            misterx = randint(0, game.count_gamers-1)
            for i, gamer in enumerate(game.gamers.all()):
                ging = GamersInGame.objects.get(game=game, gamer=gamer)
                if i == misterx:
                    ging.role = 'misterx'
                    ging.save()
                else:
                    ging.role = 'detectives'
                    ging.save()
            log.info(ging)
        elif game.count_gamers_misterx >= 2:
            misterx = randint(0, game.count_gamers_misterx-1)
            players = [player for player in game.gamers.all()
                    if GamersInGame.objects.get(game=game, gamer=player).role_misterx == True]
            for i, gamer in enumerate(players):
                ging = GamersInGame.objects.get(game=game, gamer=gamer)
                if i == misterx:
                    ging.role = 'misterx'
                    ging.save()
                else:
                    ging.role = 'detectives'
                    ging.save()
            log.info(ging)
        data = {'success': True}
        return JsonResponse(data)
    raise Http404

@login_required
def test_city(request, pk, **kwargs): # This code need to correct and update ????
    if request.method == 'POST':
        game = get_object_or_404(Game, pk=pk)
        user = request.user
        city = request.POST.get('city', None)
        ging = GamersInGame.objects.get(game=game, gamer=user)
        cities = game.get_cities
        if ging.role == 'misterx':
            if city in list(cities['detectives'].values()):
                data = {'result': 'false', 'city': city}
                # Мистер Х прoиграл
                print('------------------END------------------')
            else:
                data = {'result': 'true'}
        else:
            if city in list(cities['misterx'].values()):
                data = {'result': 'false', 'city': city}
                # ход невозможен
            else:
                data = {'result': 'true'}
                #color = request.POST.get('color')
                #game.change_cities(color=color, city=city, role='detectives')
        log.info(data)
        return JsonResponse(data)
    raise Http404

@login_required
def get_role(request, pk, **kwards): #test
    if request.method == 'GET':
        game = get_object_or_404(Game, pk=pk)
        gamer = request.user
        ging = GamersInGame.objects.get(game=game, gamer=gamer)
        role = ging.role
        data = {'role': role}
        log.info(data)
        return JsonResponse(data)
    raise Http404

@login_required
def save_city(request, pk, **kwards):
    if request.method == 'POST':
        game = get_object_or_404(Game, pk=pk)
        color = request.POST.get('color', None)
        city = request.POST.get('city', None)
        user = request.user
        ging = GamersInGame.objects.get(game=game, gamer=user)

        role = ging.role # I deleted the part of code because it is unnecessary now
        game.change_city(color=color, city=city, role=role)
        data = {'color':color, 'city':city, 'role': role}
        log.info(data)
        return JsonResponse(data)
    raise Http404

@login_required
def change_queue(request, pk, **kwargs):
    if request.method == 'POST':
        game = get_object_or_404(Game, pk=pk)
        user = request.user
        ging = GamersInGame.objects.get(game=game, gamer=user)
        role = ging.role

        game.change_queue(role);
        log.info(msg='Change_queue, new_queue: '+str(role))
        data = {'result':'true', 'role': role}
        return JsonResponse(data)
    raise Http404

@login_required
def test_queue(request, pk, **kwargs):
    if request.method == 'POST':
        game = get_object_or_404(Game, pk=pk)
        user = request.user
        queue = game.queue
        ging = GamersInGame.objects.get(game=game, gamer=user)
        role = ging.role

        data = {'queue': queue}
        if role == queue:
            data['result'] = 'true'
        else:
            data['result'] = 'false'
        log.info(msg='Test_queue, data: ' + str(data))
        return JsonResponse(data)
    raise Http404

@login_required
def add_step_of_misterx(request, pk, **kwargs):
    if request.method == 'POST':
        game = get_object_or_404(Game, pk=pk)
        new_step = request.POST.get('newStep', None)
        game.add_step_of_misterx(new_step)
        game.save()
        data = {'result':'true', 'steps': game.get_steps_of_misterx, 'count': game.count_steps}
        return JsonResponse(data)
    raise Http404

@login_required
def change_detectives_info(request, pk, **kwargs):
    if request.method == 'GET':
        game = get_object_or_404(Game, pk=pk)
        cities = game.get_cities['detectives']
        ging = GamersInGame.objects.get(game=game, gamer=request.user)
        role = ging.get_role

        data = {'result':'true', 'role': role}
        data['detectives'] = list(cities.keys())
        data['cities'] = cities
        data['count_steps'] = game.count_steps
        log.debug(data)
        return JsonResponse(data)
    raise Http404

@login_required
def get_steps_of_misterx(request, pk, **kwargs):
    if request.method == 'GET':
        game = get_object_or_404(Game, pk=pk)
        data = {'steps': game.get_steps_of_misterx, 'count': game.get_count_steps_of_misterx}
        log.debug(data)
        return JsonResponse(data)
    raise Http404

@login_required
def get_tickets(request, pk, **kwargs): # test
    if request.method == 'GET':
        game = get_object_or_404(Game, pk=pk)
        ging = GamersInGame.objects.get(game=game, gamer=request.user)
        tickets = game.get_tickets
        log.debug(ging.role)
        data = {'tickets': str(tickets[ging.role][request.user.username])}
        return JsonResponse(data)
    raise Http404

@login_required
def get_queue(request, pk, **kwargs):# test
    if request.method == 'GET':
        game = get_object_or_404(Game, pk=pk)
        queue = game.queue
        data = {'queue': str(queue)}
        log.debug(data)
        return JsonResponse(data)
    raise Http404

@login_required
def get_positions(request, pk, **kwargs):
    '''
        return {"color": position}
            with misterx if role == misterx
            else - only detectives
    '''
    if request.method == 'GET':
        game = get_object_or_404(Game, pk=pk)
        ging = GamersInGame.objects.get(game=game, gamer=request.user)
        positions = game.get_cities
        data = {}
        if ging.role == 'misterx' or len(game.get_city_way['misterx']['white'].split(',')) > 25:
            data.update(positions['misterx'])
            data.update(positions['detectives'])
        elif ging.role == 'detectives':
            data.update(positions['detectives'])

        log.debug(data)
        return JsonResponse(data)
    raise Http404

@login_required
def check_move_player(request, pk, **kwargs): # test
    '''
        input
            drag - [[num, type], ...]
            city_out, city_in
            color - color of player
            special_acts

        return {
            'result': True/False,
            'res_city': city where will be player after this step (last or next city),
            'res_tickets': tickets after this step
        }
    '''
    if request.method == 'POST':
        game = get_object_or_404(Game, pk=pk)
        ging = GamersInGame.objects.get(game=game, gamer=request.user)

        tickets = game.get_tickets[ging.role][request.user.username].split(',')

        color = request.POST.get('color', None)
        drag = request.POST.get('drag', None)
        addition_tickets = request.POST.get('special_acts', None)
        log.debug([addition_tickets, drag])

        if not drag in {None, ''}:
            drag = [x.split('+') for x in drag.split(',')]
            drag = [(int(x[0]), x[1]) for x in drag]
        else:
            drag = []
        if not addition_tickets in {None, ''}:
            addition_tickets = [x.split('+') for x in addition_tickets.split(',')]
            addition_tickets = [(int(x[0]), x[1]) for x in addition_tickets]
        else:
            addition_tickets = []

        log.info([*drag, *addition_tickets])
        if validate_action.validate_tickets([*drag, *addition_tickets], game, ging, request.user.username):
            type_step, info = steps.get_type_of_step(drag, addition_tickets, ging.role)
            log.info([type_step, info])
            if type_step == 'normal step':
                city_out = request.POST.get('start', None)
                city_in = request.POST.get('end', None)
                data = steps.normal_step(city_out, city_in, drag[0], game, ging, color, request.user.username)
            elif type_step == 'secret step':
                city_out = request.POST.get('start', None)
                city_in = request.POST.get('end', None)
                data = steps.secret_step(city_out, city_in, drag[0], addition_tickets, game, ging, color, request.user.username)
            elif type_step == 'double step':
                city1 = request.POST.get('city1', None)
                city2 = request.POST.get('city2', None)
                city3 = request.POST.get('city3', None)
                log.info([city1, city2, city3, drag, addition_tickets])
                data = steps.double_step(city1, city2, city3, drag, addition_tickets, game, ging, color, request.user.username)
            elif type_step in {'double secret step', 'secret double step'}:
                city1 = request.POST.get('city1', None)
                city2 = request.POST.get('city2', None)
                city3 = request.POST.get('city3', None)
                log.info([city1, city2, city3, drag, addition_tickets])
                data = steps.double_and_secret_step(city1, city2, city3, drag, addition_tickets, type_step, game, ging, color, request.user.username)
            elif type_step == 'addition step':
                city_out = request.POST.get('start', None)
                city_in = request.POST.get('end', None)
                data = steps.addition_step(city_out, city_in, info,addition_tickets, game, ging, color, request.user.username)
            else:
                city_out = request.POST.get('start', None)
                city_in = request.POST.get('end', None)
                data = {'result': False, 'res_city': city_out, 'res_tickets': game.get_tickets[ging.role][request.user.username]}
        else:
            city_out = request.POST.get('start', None)
            city_in = request.POST.get('end', None)
            data = {'result': False, 'res_city': city_out, 'res_tickets': game.get_tickets[ging.role][request.user.username]}
        return JsonResponse(data)
    raise  Http404

@login_required
def remove_tickets(request, pk, **kwargs):
    if request.method == 'POST':
        game = get_object_or_404(Game, pk=pk)
        ging = GamersInGame.objects.get(game=game, gamer=request.user)

        del_tickets = request.POST.get('tickets', None)
        if not game.can_move(ging.role):
            if del_tickets != None:
                del_tickets = list(map(lambda x: x.split('+'), del_tickets.split(',')))
            if validate_action.validate_del_tickets([t[1] for t in del_tickets], game, ging, request.user.username):
                game.delete_tickets(list(map(lambda x: int(x[0]), del_tickets)), ging.role, request.user.username)
        return JsonResponse({'res_tickets': game.get_tickets[ging.role][request.user.username]})
    raise Http404

@login_required
def complite_course(request, pk, **kwargs):
    if request.method == 'GET':
        game = get_object_or_404(Game, pk=pk)
        ging = GamersInGame.objects.get(game=game, gamer=request.user)
        if game.queue == ging.role:
            if not game.can_move(ging.role):
                gings = GamersInGame.objects.filter(game=game)
                if ging.role == 'detectives':
                    game.restore_tickets_for_special_act()
                for g in [g for g in gings if g.role == ging.role]:
                   g.clear_used()

                if ging.role == 'detectives':
                   game.fill_tickets_for_detectives()
                else:
                   game.fill_tickets(request.user)
                game.change_queue()
                game.add_evidences()
                ging.clear_used()
                ging.save()

        res = {'tickets': game.get_tickets[ging.role][request.user.username]}
        return JsonResponse(res)
    raise Http404

@login_required
def get_evidences(request, pk, **kwargs): # t
    if request.method == 'GET':
        game = get_object_or_404(Game, pk=pk)
        return JsonResponse({'evidences': game.get_evidences})
    return Http404

@login_required
def get_color_misterx(request, pk, **kwargs): # t
    if request.method == 'GET':
        game = get_object_or_404(Game, pk=pk)
        return JsonResponse({'color_misterx': game.color_city_misterx})
    raise Http404

@login_required
def check_end(request, pk, **kwargs): # t
    if request.method == 'GET':
        game = get_object_or_404(Game, pk=pk)
        res = {}
        if validate_action.validate_win_misterx(game):
            res = {'win': 'misterx', 'position': game.get_cities['misterx']['white']}
            game.finish_date = timezone.now()
        if validate_action.validate_win_detectives(game):
            res = {'win': 'detectives', 'position': game.get_cities['misterx']['white']}
            game.finish_date = timezone.now()
        game.save()
        return JsonResponse(res)
    raise Http404

@login_required
def get_colors(request, pk, **kwargs):
    if request.method == 'GET':
        game = get_object_or_404(Game, pk=pk)
        ging = GamersInGame.objects.get(game=game, gamer=request.user)
        data = {'colors': ', '.join(map(str, ging.get_colors))}
        return JsonResponse(data)
    raise Http404

@login_required
def get_tickets_for_special_act(request, pk, **kwargs):
    if request.method == 'GET':
        game = get_object_or_404(Game, pk=pk)
        ging = GamersInGame.objects.get(game=game, gamer=request.user)
        tickets = game.get_all_tickets_for_special_act
        tickets.sort()
        res = {'tickets_for_special_act': ','.join(tickets)}
        return JsonResponse(res)
    raise Http404


@login_required
def clear_special_tickets(request, pk, **kwargs):
    if request.method == 'POST':
        game = get_object_or_404(Game, pk=pk)
        ging = GamersInGame.objects.get(game=game, gamer=request.user)
        game.restore_tickets_for_special_act()
        return JsonResponse({'tickets': game.get_tickets[ging.role][request.user.username]})
    raise Http404

@login_required
def double_step(request, pk, **kwargs):
    """
    Params:
        mode: { 'standart_standart', 'standart_secret', 'secret_standart' }
        special_tickets: билеты для спецдействия
        ticket1, ticket2: билеты для перемещения в таком порядке
        city1, city2: города
    """
    if request.method == 'POST':
        game = get_object_or_404(Game, pk=pk)
        ging = GamersInGame.objects.get(game=game, gamer=request.user)

        mode = request.POST.get('mode', None)
        special_tickets = request.POST.get('special_tickets', None)
        ticket1 = request.POST.get('ticket1', None)
        ticket2 = request.POST.get('ticket2', None)
        city1 = request.POST.get('city1', None)
        city2 = request.POST.get('city2', None)

        res = False
        if validate_action.validate_correct_data_in_double_step(\
                mode, special_tickets, ticket1, ticket2, city1, city2):
            parser_ticket = lambda x: (int(x.split('+')[0]), x.split('+')[1])
            special_tickets = list(map(parser_ticket, special_tickets.split(',')))
            ticket1 = parser_ticket(ticket1)
            ticket2 = parser_ticket(ticket2)
            city1 = int(city1)
            city2 = int(city2)

            if mode == 'standart_secret':
                special_tickets = \
                    [t for t in special_tickets if 'double' in t[1]] + \
                    [t for t in special_tickets if 'secret' in t[1]]
            elif mode == 'secret_standart':
                special_tickets = \
                    [t for t in special_tickets if 'secret' in t[1]] + \
                    [t for t in special_tickets if 'double' in t[1]]

            step_type = steps.get_type_of_step([ticket1, ticket2], special_tickets, 'misterx')[0]
            if step_type == 'double step' and mode == 'standart_standart':
                data = steps.double_step(game.get_cities['misterx']['white'], city1, city2,\
                        [ticket1, ticket2], special_tickets,\
                        game, ging, 'white', request.user.username)
                res = data['result']
            elif (step_type == 'double secret step' and mode == 'standart_secret') or \
                    (step_type == 'secret double step' and mode == 'secret_standart'):
                data = steps.double_and_secret_step(game.get_cities['misterx']['white'], city1, city2,\
                        [ticket1, ticket2], special_tickets, step_type,\
                        game, ging, 'white', request.user.username)
                res = data['result']

        return JsonResponse({'result': res})
    raise Http404

@login_required
def secret_step(request, pk, **kwargs): # t
    """
    Params:
        special_tickets: билеты для спецдействия
        ticket: билет для перемещения
        city: город назначения
    """
    if request.method == 'POST':
        game = get_object_or_404(Game, pk=pk)
        ging = GamersInGame.objects.get(game=game, gamer=request.user)
        res = False

        special_tickets = request.POST.get('special_tickets', None)
        ticket = request.POST.get('ticket', None)
        city = request.POST.get('city', None)
        res = False

        if special_tickets and ticket and special_tickets != '' and ticket != '':
            parser_tickets = lambda x: (int(x.split('+')[0]), x.split('+')[1])
            special_tickets = list(map(parser_tickets, special_tickets.split(',')))
            ticket = parser_tickets(ticket)

            data = steps.secret_step(game.get_cities['misterx']['white'], city, ticket, special_tickets, game, ging, 'white', request.user.username)
            res = data['result']

        return JsonResponse({'result': res})
    raise Http404

@login_required
def search_step(request, pk, **kwargs):
    """
    Params:
        nothing
    """
    if request.method == 'POST':
        game = get_object_or_404(Game, pk=pk)
        ging = GamersInGame.objects.get(game=game, gamer=request.user)
        tickets = game.get_all_tickets_for_special_act
        data = {'result': False}
        step = steps.get_type_of_step([], tickets, ging.role)

        if step[0] == 'search step' and \
                game.queue == ging.role == 'detectives' and \
                len(game.get_city_way['misterx']['white']) >= 5:
                    game.add_evidence_past(step[1])

                    game.delete_tickets_for_special_act();

                    data['result'] = True
        return JsonResponse(data)
    raise Http404

@login_required
def addition_step(request, pk, **kwargs):
    """
    Params:
        color: цвет детектива
        city: город, куда перемещается детектив
    """
    if request.method == 'POST':
        game = get_object_or_404(Game, pk=pk)
        ging = GamersInGame.objects.get(game=game, gamer=request.user)
        data = {'result': False}

        color = request.POST.get('color', None)
        to_city = request.POST.get('city', None)
        from_city = game.get_cities['detectives'][color]

        tickets = game.get_all_tickets_for_special_act
        step = steps.get_type_of_step([], tickets, ging.role)

        if step[0] == 'addition step':
            transport_type = step[1]
            data['result'] = steps.addition_step(from_city, to_city, transport_type, game, ging, color, request.user.username)['result']
        game.save()

        return JsonResponse(data)
    raise Http404

@login_required
def add_special_tickets(request, pk, **kwargs):
    """
    Params:
        tickets: билеты
        type_of_action: тип действия для которого собираются билеты
    """
    if request.method == 'POST':
        game = get_object_or_404(Game, pk=pk)
        ging = GamersInGame.objects.get(game=game, gamer=request.user)
        data = {'result': False}

        if game.queue == ging.role:
            if (ging.role == 'detectives' and (not game.can_move(ging.role))) or ging.role != 'detectives':
                if game.type_of_special_act == '':
                    game.type_of_special_act = request.POST.get('type_of_action', '')

                tickets = request.POST.get('tickets', None)
                res = game.add_tickets_for_special_act(request.user.username, tickets.split(','))
                data['result'] = res

        return JsonResponse(data)
    raise Http404

@login_required
def get_possible_steps(request, pk, **kwargs):
    """
    Params: nothing
    Return: {'result': True/False, 'can_move': []}
    """
    if request.method == 'GET':
        game = get_object_or_404(Game, pk=pk)
        ging = GamersInGame.objects.get(game=game, gamer=request.user)
        data = {'result': False, 'can_move': []}
        players = [player for player in game.gamers.all()
                if GamersInGame.objects.get(game=game, gamer=player).role == ging.role]

        res = game.can_move(ging.role)
        data['result'] = res
        if res:
            moveable_colors = []
            for player in players:
                available_cities = game.get_available_cities(player)
                for color in available_cities.keys():
                    if available_cities[color] != []:
                        moveable_colors.append(color)
            data['can_move'] = moveable_colors 

        return JsonResponse(data)
    raise Http404

@login_required
def add_message(request, pk, **kwargs):
    """
    Params:
        time, username, message_text
    """
    if request.method == 'POST':
        game = get_object_or_404(Game, pk=pk)

        time = request.POST.get('time', None)
        username = request.user.username
        text = request.POST.get('text', None)

        game.new_message(time, username, text)
        return JsonResponse({'res': True, 'chat': game.get_chat})
    raise Http404


@login_required
def get_chat(request, pk, **kwargs):
    """
    Params:
        ---
    """
    if request.method == 'GET':
        game = get_object_or_404(Game, pk=pk)
        return JsonResponse(game.get_chat, safe=False)
    raise Http404

