from django.conf import settings


'''
There is a dictionary with translations of phrases
'''
dict_phrases = {
        'Home': 'Начало',
        'Profile': 'Профиль',
        'Games': 'Игры',
        'New game': 'Новая игра',
        'Game title': 'Название игры',
        'Number of players': 'Количество игроков',
        'Create game': 'Создать игру',
        'Reset': 'Очистить',
        'Detail of game': 'Подробности',
        'Date created': 'Дата создания',
        'Date finished': 'Дата окончания',
        'Number of gamers': 'Текущее количество игроков',
        'Maximum of gamers': 'Максимальное число игроков',
        'Take part': 'Принять участие',
        'You will not be added to this game.': 'Вы не можете принять участие в этой игре',
        'Setup game': 'Подготовка к игре',
        'You are': 'Ваш логин',
        'Color of Mr.X cities': 'Цвет города Mr.X',
        'Gamer': 'Игрок',
        'Status': 'Статус',
        'Desire to be Mr.X': 'Хочет быть Mr.X',
        'Ready': 'Готов',
        'Not ready': 'Не готов',
        'Ready ': 'Готова',
        'Not ready ': 'Не готова',
        'green': 'зеленый',
        'yellow': 'желтый',
        'orange': 'оранжевый',
        'No games are available.': 'Нет доступных игр.',
        'End of game': 'Конец игры',
        'This game finished': 'Эта игра закончилась',
        'Win': 'Победила команда',
        'MisterX in': 'Мистер Х в городе',
        'Waiting': 'Подождите',
        'It is uploading': 'Идет загрузка',
        'Back to Games': 'Назад к играм',
        'Short rules': 'Краткие правила',
        'Color of MisterX city': 'Цвет городов Mr.X',
        'Your colors': 'Ваши цвета',
        'You are': 'Ваша роль',
        'Your login': 'Ваш логин',
        'Queue': 'Очередь',
        'Steps of Mister X': 'Ходы Мистера Х',
        'Additional fields': 'Дополнительные поля',
        'Evidences': 'Улики',
        'Chat': 'Чат',
        'step': 'ход',
        'city': 'город',
        'Map': 'Карта',
        'Tickets': 'Билеты',
        'Delete tickets': 'Удалить билеты',
        'Complete course': 'Закончить ход',
        'Special acts': 'Специальные действия',
        'Acts': 'Открыть меню',
        'Cancel': 'Отменить',
        'Menu': 'Меню',
        }

def translate(key):
    if settings.LANGUAGE_CODE == 'ru-ru':
        if key in dict_phrases:
            return dict_phrases[key]
        else:
            return key
    else:
        return key

def translate_color(color):
    if settings.LANGUAGE_CODE == 'ru-ru':
        return {
            'зеленый'  : 'green' ,
            'желтый'   : 'yellow',
            'оранжевый': 'orange',
            'green'  : 'зеленый'  ,
            'yellow' : 'желтый'   ,
            'orange' : 'оранжевый',
            }[color]
    else:
        return color

def make_context(keys):
    context = { tag_name: translate(keys[tag_name]) for tag_name in keys }
    return context

def get_context_for_navbar():
    keys = {'tag_home': 'Home', 'tag_profile': 'Profile', 'tag_games': 'Games'}
    return make_context(keys)

def get_context_for_create_game():
    keys = {'tag_new_game': 'New game', 'tag_game_title': 'Game title', 'tag_numbers': 'Number of players', 'tag_create': 'Create game', 'tag_reset': 'Reset'}
    return make_context(keys)

def get_context_for_detail_game():
    keys = {'tag_detail': 'Detail of game',
            'tag_created': 'Date created',
            'tag_finished': 'Date finished',
            'tag_number': 'Number of gamers',
            'tag_max': 'Maximum of gamers',
            'tag_take_part': 'Take part',
            'tag_not_take': 'You will not be added to this game.'
            }
    return make_context(keys)

def get_context_for_setup():
    keys = {'tag_setup': 'Setup game',
            'tag_login': 'Your login',
            'tag_color': 'Color of Mr.X cities',
            'tag_gamer': 'Gamer',
            'tag_status': 'Status',
            'tag_desire': 'Desire to be Mr.X',
            'tag_ready': 'Ready',
            'tag_not_ready': 'Not ready',
            'tag_ready_w': 'Ready ',
            'tag_not_ready_w': 'Not ready ',
            'tag_green': 'green',
            'tag_yellow': 'yellow',
            'tag_orange': 'orange',
            }
    return make_context(keys)

def get_context_for_index():
    keys = {'tag_index': 'Games',
            'tag_create': 'Create game',
            'tag_empty': 'No games are available.',
            }
    return make_context(keys)

def get_context_for_end():
    keys = {'tag_end': 'End of game',
            'tag_finish': 'This game finished',
            'tag_win': 'Win',
            'tag_position': 'MisterX in',
            }
    return make_context(keys)

def get_context_for_main():
    keys = {'tag_waiting': 'Waiting',
            'tag_upload': 'It is uploading',
            'tag_back': 'Back to Games',
            'tag_rules': 'Short rules',
            'tag_mx_color': 'Color of MisterX city',
            'tag_colors': 'Your colors',
            'tag_role': 'You are',
            'tag_queue': 'Queue',
            'tag_steps': 'Steps of Mister X',
            'tag_add': 'Additional fields',
            'tag_evidences': 'Evidences',
            'tag_chat': 'Chat',
            'tag_step': 'step',
            'tag_city': 'city',
            'tag_map': 'Map',
            'tag_tickets': 'Tickets',
            'tag_del': 'Delete tickets',
            'tag_complete': 'Complete course',
            'tag_special': 'Special acts',
            'tag_acts': 'Acts',
            'tag_cancel': 'Cancel',
            'tag_menu': 'Menu',
            }
    return make_context(keys)

