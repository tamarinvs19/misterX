from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model
from datetime import timedelta

import logging as log
from random import randint, shuffle
from itertools import product

from . import generation
from . import steps
from . import data


log.basicConfig(
        level='DEBUG',
        #%(name)s   %(asctime)s
        format='%(asctime)s  %(levelname)s  %(filename)s  %(funcName)s  --> %(message)s',
        filename='./misterx.log',
        filemode='a',
        )

User = get_user_model()


class Game(models.Model):
    game_name = models.CharField(max_length=200)
    create_date = models.DateTimeField('date created')
    finish_date = models.DateTimeField('date finished')
    chat = models.TextField(default="[]")

    start_setting_status = models.BooleanField(default=False)

    gamers = models.ManyToManyField(User, through='GamersInGame')
    queue = models.TextField(default='misterx')
    max_count_players = models.IntegerField(default=2)

    color_city_misterx = models.TextField(default='green')

    cities = models.TextField(
            default="{'misterx':{'white':'0'},'detectives':{'green':'0','red':'0','blue':'0','purple':'0','orange':'0'}}")
    tickets = models.TextField(
            default="{'misterx':{'gamer1':''},'detectives':{'gamer2':''}}")

    used_tickets = models.TextField(
            default="{'misterx':{'white':''},'detectives':{'green':'','red':'','blue':'','purple':'','orange':''}}")
    city_way = models.TextField(
            default="{'misterx':{'white':''},'detectives':{'green':'','red':'','blue':'','purple':'','orange':''}}")

    evidences = models.TextField(default='')

    type_of_special_act = models.TextField(default='')
    tickets_for_special_act = models.TextField(default="{'login1':[],'login2':[]}")

    count_tickets_for_special_act = {'addition_car': 3, 'addition_train': 4, 'addition_plane': 5, 'evidence_2': 5, 'evidence_3': 4, 'evidence_4': 3}

    def __str__(self):
        return self.game_name

    @property
    def count_gamers(self):
        return len(self.gamers.all())

    @property
    def count_gamers_misterx(self):
        count = 0
        for g in self.gamers.all():
            ging = GamersInGame.objects.get(game=self, gamer=g)
            if ging.role == 'misterx':
                count += 1
        return count

    @property
    def can_gamers_add(self):
        res = self.count_gamers < self.max_count_players and self.finish_date >= timezone.now()
        return res

    @property
    def are_all_gamers_ready(self):
        lobby = GamersInGame.objects.filter(game=self)
        res = [l.ready for l in lobby]
        res = self.can_gamers_add == False and res == [True for _ in len(res)]
        return res

    @property
    def get_cities(self):  # +
        return dict(eval(self.cities))

    @property
    def get_tickets(self): # +
        return dict(eval(self.tickets))

    @property
    def get_used_tickets(self): # +
        return dict(eval(self.used_tickets))

    @property
    def get_city_way(self):  # +
        return dict(eval(self.city_way))

    @property
    def get_steps_of_misterx(self): # +
        mx_steps = [s for s in self.get_used_tickets['misterx']['white'].split(',')]
        if 'None' in mx_steps or '' in mx_steps:
            mx_steps = mx_steps[1:]
        return mx_steps

    @property
    def get_count_steps_of_misterx(self):  # +
        return len(self.get_steps_of_misterx)

    @property
    def get_evidences(self):
        return self.evidences.split(',')

    @property
    def get_tickets_for_special_act(self): # +
        return dict(eval(self.tickets_for_special_act))

    @property
    def get_all_tickets_for_special_act(self): 
        return [t.split('+')[1] for tickets in self.get_tickets_for_special_act.values() \
                for t in tickets]

    def can_move(self, role):
        """
        role = 'misterx' / 'detectives'
        return: True / False
        """
        result = True

        players = self.get_players_by_role(role)
        for player in players:
            if not all(cities == [] for cities in self.get_available_cities(player).values()):
                return True
        return False

    def get_available_cities(self, player):
        """ 
        return: {'<color>': [list of available cities] }
        """
        ging = GamersInGame.objects.get(game=self, gamer=player)
        type_tickets = [\
                ticket.split('_')[0] \
                for ticket in self.get_tickets[ging.role][player.username].split(',') \
                if ticket != '' ]
        colors = ging.get_colors
        unused_colors = [c for c in colors if c not in ging.get_used]
        cities_out = {color: int(city) for color, city in self.get_cities[ging.role].items()}

        available_cities = {color: [] for color in colors}
        for color in unused_colors:
            for connection in data.connections:
                connect_cities = connection['city1'], connection['city2']

                check_cities = cities_out[color] in connect_cities
                check_connection = connection['type'] in type_tickets 
                check_other_players = all([
                    city == cities_out[color] or city not in connect_cities \
                            for city in cities_out.values()])
                if check_cities and check_connection and check_other_players:
                    available_cities[color].append(\
                            connect_cities[0] if connect_cities[0] != cities_out[color] else connect_cities[1])
        return available_cities

    def get_players_by_role(self, role):
        res_players = []
        for player in self.gamers.all():
            ging = GamersInGame.objects.get(game=self, gamer=player)
            if ging.role == role:
                res_players.append(player)
        return res_players

    def check_tickets_cnt(self, login, used_tickets_cnt, new_tickets_cnt):
        player = self.gamers.get(username=login)
        ging = GamersInGame.objects.get(game=self, gamer=player)
        max_cnt = len(ging.get_colors)
        return (used_tickets_cnt + new_tickets_cnt) <= max_cnt

    def add_tickets_for_special_act(self, login, new_tickets): 
        all_tickets = self.get_all_tickets_for_special_act
        types_tickets = {t.split('_')[1] for t in all_tickets}
        new_types_tickets = {t.split('_')[1] for t in new_tickets}
        used_tickets = self.get_tickets_for_special_act
        used_tickets_cnt = len(used_tickets[login])
        new_tickets_cnt = len(new_tickets)

        check_user_count = self.check_tickets_cnt(login, used_tickets_cnt, new_tickets_cnt)
        check_count = len(all_tickets) + len(new_tickets) <= self.count_tickets_for_special_act[self.type_of_special_act]
        check_count_type = len(new_types_tickets) == 1 and \
                (types_tickets == new_types_tickets or types_tickets == set())
        if 'evidence' in  self.type_of_special_act:
            check_type = 'search' in list(new_types_tickets)[0]
        elif 'addition' in self.type_of_special_act:
            check_type = 'addition' in list(new_types_tickets)[0]
        else:
            check_type = False

        log.debug(f'Type of special act: {self.type_of_special_act}, new type: {list(new_types_tickets)}')
        if check_count and check_count_type and check_type and check_user_count:
            tickets = self.get_tickets['detectives'][login].split(',')
            for ticket in new_tickets:
                num, body = int(ticket.split('+')[0]), ticket.split('+')[1]
                if body != tickets[num]:
                    return False

            special_tickets = self.get_tickets_for_special_act
            special_tickets[login] += new_tickets

            self.tickets_for_special_act = str(special_tickets)
            self.delete_tickets([int(t.split('+')[0]) for t in new_tickets], role='detectives', username=login)
            self.save()
            return True
        else:
            return False

    def add_evidences(self): # +
        cities = [str(c) for c in self.get_cities['detectives'].values()]
        mx_way = self.get_city_way['misterx']['white'].split(',')
        f = lambda i, x: x if i >= len(mx_way) - 5 else 0
        mx_way = [f(i, x) for i, x in enumerate(mx_way)]
        evidences = [x.split('+') for x in self.get_evidences if x != '']
        evidences = [[int(x[0]), int(x[1])] for x in evidences]
        for c in cities:
            if c in mx_way:
                step = [mx_way.index(c), int(c)]
                if step not in evidences:
                    evidences.append(step)
        evidences.sort()
        self.evidences = ','.join(map(lambda e: str(e[0]) + '+' + str(e[1]), evidences))
        self.save()
        log.debug([evidences, self.evidences])

    def add_evidence_past(self, count): #+
        city = self.get_city_way['misterx']['white'].split(',')
        evidences = [x.split('+') for x in self.get_evidences if x != '']
        evidences = [[int(x[0]), int(x[1])] for x in evidences]
        new_evi = [len(city) - count-1, int(city[-count-1])]
        if new_evi not in evidences:
            evidences.append(new_evi)
        evidences.sort()
        self.evidences = ','.join(map(lambda e: str(e[0]) + '+' + str(e[1]), evidences))
        self.save()

    def add_used_ticket(self, new_ticket, role='misterx', color='white'):  # +
        used_tickets = self.get_used_tickets
        if used_tickets[role][color] != '':
            used_tickets[role][color] += (',' + str(new_ticket))
        else:
            used_tickets[role][color] += str(new_ticket)
        self.used_tickets = str(used_tickets)
        self.save()
        log.info(self.used_tickets)

    def add_tickets(self, new_tickets, role, username):
        ''' new_tickets -example- ['car_secret', ...] '''
        all_tickets = self.get_tickets
        tickets = all_tickets[role][username].split(',')
        log.info(['new_tickets:', new_tickets, 'tickets:', tickets, 'all:', all_tickets])
        if new_tickets != [] and new_tickets != ['']:
            i = 0
            while i < len(tickets) and len(new_tickets) > 0:
                if tickets[i] == '':
                    tickets[i] = new_tickets[0]
                    del new_tickets[0]
                i += 1
            if len(new_tickets) > 0:
                tickets += new_tickets

            res = ''
            for t in tickets:
                res += (t + ',')
            res = res[:-1]

            all_tickets[role][username] = res
            self.tickets = str(all_tickets)
            self.save()

    def add_gamer(self, gamer):  # +
        if len(self.gamers.filter(id=gamer.id)) == 0:
            GamersInGame.objects.create(game=self, gamer=gamer)
        return self

    def restore_tickets_for_special_act(self):
        tickets = self.get_tickets_for_special_act
        for login in tickets.keys():
            self.add_tickets([t.split('+')[1] for t in sorted(tickets[login]) if '+' in t], 'detectives', login)
            tickets[login] = []
        self.tickets_for_special_act = str(tickets)
        self.type_of_special_act = ''
        self.save()

    def delete_tickets_for_special_act(self):
        tickets = self.get_tickets_for_special_act
        for login in tickets.keys():
            tickets[login] = []
        self.tickets_for_special_act = str(tickets)
        self.type_of_special_act = ''
        self.save()

    def delete_tickets(self, numbers, role='misterx', username=''):
        all_tickets = self.get_tickets
        tickets = all_tickets[role][username].split(',')
        for n in numbers:
            tickets[n] = ''

        res = ''
        for t in tickets:
            res += (t + ',')
        res = res[:-1]

        all_tickets[role][username] = res
        self.tickets = str(all_tickets)
        self.save()

    def change_city(self, color, city, role):  # +
        cities = self.get_cities
        city_way = self.get_city_way
        if city_way[role][color] != '':
            city_way[role][color] += (',' + str(city))
        else:
            city_way[role][color] = str(city)

        cities[role][color] = city
        self.cities = str(cities)
        self.city_way = str(city_way)
        self.save()
        log.info([self.cities, self.city_way])

    def change_queue(self):  # +
        if self.queue == 'misterx':
            self.queue = 'detectives'
        else:
            self.queue = 'misterx'
        self.save()

    def new_message(self, datetime, username, text):
        new_chat = self.get_chat
        new_message = {"time": datetime, "author": username, "text": text}
        new_chat.append(new_message)
        self.chat = str(new_chat)
        self.save()

    def fill_tickets(self, player): # +
        ging = GamersInGame.objects.get(game=self, gamer=player)
        count_tickets = sum(1 for ticket in self.get_tickets[ging.role][player.username].split(',') if ticket != '')
        self.add_tickets(generation.generate_new_tickets(ging.get_count_tickets - count_tickets), ging.role, ging.gamer.username)

    def fill_tickets_for_detectives(self): # +
        gings = GamersInGame.objects.filter(game=self)
        players = [g.gamer for g in gings if g.role == 'detectives']
        for p in players:
            self.fill_tickets(p)
        for g in [g for g in gings if g.role == 'detectives']:
            g.clear_used()
            g.save()

    def choice_misterx(self): #+
        tickets = {'misterx': {}, 'detectives':{}}
        colors = ['red', 'orange', 'green', 'blue', 'purple']
        shuffle(colors)

        if self.max_count_players == 2:
            detective_colors = (colors, [])
        elif self.max_count_players == 3:
            detective_colors = (colors[0:3], colors[3:5])
        elif self.max_count_players == 4:
            detective_colors = (colors[0:2], colors[2:4], [colors[4]])
        elif self.max_count_players == 5:
            detective_colors = (colors[0:2], [colors[2]], [colors[3]], [colors[4]])
        else:
            detective_colors = ([color] for color in colors)

        detective_colors = iter(detective_colors)

        if self.count_gamers_misterx == 0:
            misterx = randint(0, self.count_gamers-1)
            players = self.gamers.all()
        else:
            misterx = randint(0, self.count_gamers_misterx-1)
            players = [player for player in self.gamers.all()
                    if GamersInGame.objects.get(game=self, gamer=player).role == 'misterx']
            players += [player for player in self.gamers.all() if player not in players]

        for i, gamer in enumerate(players):
            ging = GamersInGame.objects.get(game=self, gamer=gamer)
            if i == misterx:
                ging.role = 'misterx'
                ging.colors = str(['white'])
                tickets['misterx'][str(gamer.username)] = ''
                self.color_city_misterx = ging.color_misterx
            else:
                ging.role = 'detectives'
                ging.colors = str(next(detective_colors))
                tickets['detectives'][str(gamer.username)] = ''
            ging.save()
        self.tickets = str(tickets)
        self.save()

    def generate_players_positions(self):
        cities = self.get_cities
        cities['misterx']['white'] = generation.generate_city_for_misterx(self.color_city_misterx)
        cities['detectives'] = {k: c for k, c in zip(cities['detectives'].keys(), generation.generate_cities_for_detectives())}
        self.cities = str(cities)
        log.debug(['--------------', self.cities])
        self.save()

    def init_tickets_for_special_act(self):
        res = {}
        for ging in GamersInGame.objects.filter(game=self, role='detectives'):
            res[ging.gamer.username] = []
        self.tickets_for_special_act = str(res)
        self.save()

    def initialization(self):
        self.choice_misterx()

        #fill tickets
        for ging in GamersInGame.objects.filter(game=self):
            self.fill_tickets(ging.gamer)

        # init special tickets
        self.init_tickets_for_special_act()

        # generate positions
        self.generate_players_positions()

        # save status
        self.start_setting_status = True
        self.save()

    def was_created_recently(self):
        now = timezone.now()
        return now - timedelta(days=1) <= self.create_date <= now

    @property
    def get_chat(self):
        return eval(self.chat)

    was_created_recently.admin_order_field = 'create_date'
    was_created_recently.boolean = True
    was_created_recently.short_description = 'Created recently?'

    class Meta:
        ordering = ['-create_date']


class GamersInGame(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    gamer = models.ForeignKey(User, on_delete=models.CASCADE)
    ready = models.BooleanField(default=False)
    color_misterx = models.TextField(default='green')

    role = models.TextField(default='detectives')
    colors = models.TextField(default="['red', 'orange', 'green', 'blue', 'purple']")
    used = models.TextField(default="")

    def __str__(self):
        return '  game: ' + str(self.game) +\
                ', gamer: ' + str(self.gamer) +\
                ', ready: ' + str(self.ready) +\
                ', color_mx: ' + str(self.color_misterx) +\
                ', role: ' + str(self.role)+\
                ', colors: ' + str(self.colors) +\
                ', used: ' + str(self.used)

    @property
    def get_colors(self):
        return eval(self.colors)

    @property
    def get_count_tickets(self):
        if self.role == 'misterx':
            return 8
        else:
            return 4 + len(self.get_colors)

    @property
    def get_used(self):
        return set(self.used.split(','))

    def add_used(self, color):
        if self.used == '':
            self.used += color
        else:
            self.used += ',' + str(color)
        self.save()

    def clear_used(self):
        self.used = ""
        self.save()

