from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    gender = models.TextField('gender', unique=False, default='')

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'gender']

    def __str__(self):
        return self.username

