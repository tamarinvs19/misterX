from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm
from django.utils import timezone
from django.http import HttpResponse

from collections import Counter

from .models import CustomUser
from .forms import CustomUserSettingsGender
from games.models import GamersInGame
from games import validate_action


@login_required
def my_view_auth(request):
    user = request.user
    user_games = [ging.game for ging in GamersInGame.objects.filter(gamer=user)]
    count_games = len(user_games)

    def check_win(game, role):
        if role == 'misterx':
            return validate_action.validate_win_misterx(game)
        else:
            return validate_action.validate_win_detectives(game)

    finished_games = [check_win(ging.game, ging.role) 
            for ging in GamersInGame.objects.filter(gamer=user)]
    count_win = Counter(finished_games)[True]

    role = [ging.role for ging in GamersInGame.objects.filter(gamer=user)]
    count_misterx = Counter(role)['misterx']
    count_detectives = Counter(role)['detectives']

    context = {
            'active_games': [game for game in user_games 
                if game.finish_date > timezone.now()],
            'count_games': count_games,
            'count_win': count_win,
            'count_misterx': count_misterx,
            'count_detectives': count_detectives,
            }
    return render(request, 'users/user_profile.html', context)

@login_required
def settings(request):
    if request.method == "GET":
        form = CustomUserSettingsGender()
        return render(request, 'users/settings.html', {'form': form})
    elif request.method == "POST":
        gender = request.POST.get('gender', None)
        if gender != None:
            request.user.gender = gender
            request.user.save()
        return redirect('/accounts/profile')
    else:
        raise Http404


