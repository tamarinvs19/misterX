from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import CustomUser


class CustomSignupForm(forms.Form):
    first_name = forms.CharField(label="Имя", max_length=30, widget=forms.TextInput(attrs={'placeholder': 'Имя'}))
    last_name = forms.CharField(label="Фамилия", max_length=30, widget=forms.TextInput(attrs={'placeholder': 'Фамилия'}))

    GENDERS = (('man', 'М'), ('woman', 'Ж'))
    gender = forms.ChoiceField(label='Пол', choices=GENDERS, widget=forms.Select())

    def signup(self, request, user):
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.gender = self.cleaned_data['gender']

        user.save()

class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = CustomUser
        fields = ('username', 'first_name', 'last_name', 'email', 'gender', 'date_joined',)

class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = ('username', 'first_name', 'last_name', 'gender',)

class CustomUserSettingsGender(forms.Form):
    GENDERS = (('man', 'М'), ('woman', 'Ж'))
    gender = forms.ChoiceField(label='Пол', choices=GENDERS, widget=forms.Select())

