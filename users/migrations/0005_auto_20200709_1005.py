# Generated by Django 2.2 on 2020-07-09 07:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_auto_20200709_1002'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customuser',
            name='second_name',
        ),
        migrations.AlterField(
            model_name='customuser',
            name='first_name',
            field=models.CharField(blank=True, max_length=30, verbose_name='first name'),
        ),
        migrations.AlterField(
            model_name='customuser',
            name='last_name',
            field=models.CharField(default='', max_length=30, verbose_name='last name'),
        ),
    ]
