from django.shortcuts import render

from games import translation_text


def main(request):
    return render(request, 'website/page.html', translation_text.get_context_for_navbar())

def contacts(request):
    return render(request, 'website/contacts.html')

def rules(request):
    return render(request, 'rules/Rules.html')

def short(request):
    return render(request, 'rules/shortRules.html')

def special_acts(request):
    return render(request, 'rules/special_acts.html')

