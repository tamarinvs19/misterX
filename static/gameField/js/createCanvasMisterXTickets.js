function createCanvasMisterXTickets(){
	let app = new Application({
		width: 256,         // default: 800
		height: 256,        // default: 600
		antialias: true,    // default: false
		transparent: false, // default: false
		resolution: 1       // default: 1
	}
	);
	scale = getMXTicketsScale();
	size = standartWidths['ticketMX'] * scale;
	console.log(scale, size)

	// Настройка полотна
	app.steps = []

	app.renderer.autoResize = true;
	if (reverse == false) {
		app.renderer.resize(stepsFieldSize['x'], stepsFieldSize['y'] );
	} else {
		height = (stepsFieldSize['x'] - 40) / 8 * 6 * 0.75 ;
		app.renderer.resize(stepsFieldSize['x'], height);
	}
	app.renderer.backgroundColor = 0x777777;
	div.appendChild(app.view);
	document.canvasMisterXTickets = app;

	// Подключение текстур
	app.texture = resources[nameTextureTickets].textures;
	// Создание клеток для билетов
	for (i=0; i<24; i+=1){
		let rectangle = new PIXI.Graphics();

		rectangle.lineStyle(1, 0x333333, 1);
		rectangle.beginFill(0x999999);
		x = (i % 3) * (2*size + 10) + 5;
		y = Math.trunc(i / 3) * 4/3 *(size + 5) + 20*scale;
		if (reverse) {
			rectangle.drawRect(y, x, size, 2*size);
		} else {
			rectangle.drawRect(x, y, 2*size, size);
		}

		rectangle.endFill();
		app.stage.addChild(rectangle);

		let number = new PIXI.Graphics();
		number.lineStyle(1, 0x333333, 1);
		number.beginFill(0x999999);
		x = (i % 3) * (2*size + 10) + 5;
		y = Math.trunc(i / 3) * 4/3 * (size + 5) +10*scale;
		if (reverse) {
			number.drawRect(y, x, 12*scale, 18*scale);
		} else {
			number.drawRect(x, y, 25*scale, 12*scale);
		}
		number.endFill();
		app.stage.addChild(number);

		color = "white";
		if (i == 12) {color = 'green'}

		let style = new PIXI.TextStyle({
			fontFamily: "Hack",
			fontSize: Math.trunc(10*scale),
			fill: color,
			align: 'center',
		});

		let message = new PIXI.Text(Math.trunc(i/3) + 8*(i%3) +1, style);

		if (reverse) {
			message.position.set(y + 1*scale, x+4*scale);
			message.anchor.set(1, 0);
			message.rotation = 4.71;
		} else {
			message.position.set(x+4*scale, y + 1*scale);
		}
		app.stage.addChild(message);

	}

	console.log('canvas mx tickets created');
}
