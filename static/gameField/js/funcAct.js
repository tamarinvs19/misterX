function secretStep() {
	let selectedTickets = document.canvasTickets.dragTickets;
	let count = selectedTickets.reduce(
		(sum, ticket) => (ticket.split('_')[1] == 'secret' ? 1 : 0) + sum,
		0);
	if (count == 3 &&
		document.queueRole == document.roleGamer &&
		document.canvasTickets.dragTickets.length == 3) {
		menu.mode = 'secret';
		document.canvasTickets.dragTickets.forEach((item, i, arr) => menu.specialTickets.push(arr[i]));
		document.canvasTickets.dragTickets.forEach((item, i, arr) => document.getElementById(arr[i]).style['outline-color'] = '#af0000');
		clearDragTickets();
	}
	$(menu.div).css("visibility", "hidden");
	appSpecialActs.text = "Выберите билет для перемещения и перетащите фигурку Мистера Х";
	console.log('secret_step');
}

function standartDoubleStep() {
	document.doubleStepType = 'standart_standart';
	doubleStep();
}

function secretDoubleStep() {
	document.doubleStepType = 'secret_standart';
	doubleStep();
}

function doubleSecretStep() {
	document.doubleStepType = 'standart_secret';
	doubleStep();
}

function doubleStep() {
	let selectedTickets = document.canvasTickets.dragTickets;
	if (menu.mode == 'choice' &&
		document.queueRole == document.roleGamer &&
		document.doubleStep == false 
		) {

		document.doubleStepCount = 0;
		document.doubleStep = true;
		document.doubleTickets = [];

		startCity = document.canvasMap.players[0].numCity;
		document.doublePath = [startCity];
		menu.mode = 'double';
		menu.specialTickets = [];

		document.canvasTickets.dragTickets.forEach((item, i, arr) => menu.specialTickets.push(arr[i]));
		document.canvasTickets.dragTickets.forEach((item, i, arr) => document.getElementById(arr[i]).style['outline-color'] = '#af0000');
		clearDragTickets();
	}
	$(menu.div).css("visibility", "hidden");
	appSpecialActs.text = "Дважды выберите билет для перемещения и перетащите фигурку Мистера Х";
	console.log('double_step');
}

function addDoubleStep(ticket, city) {
	if (document.doubleStep == true) {
		if (document.doubleStepCount < 2) {
			document.doubleStepCount += 1;
			document.doubleTickets.push(ticket);
			document.doublePath.push(city);
		}
		if (document.doubleStepCount == 2) {
			document.doubleStep = false;
		}
	}
}

function searchStep(backCount) {
	menu.openSpecialTickets();
	menu.type_of_action = `evidence_${backCount}`;
	console.info('search step', backCount);
}

function useAdditionStep(type) {
	menu.openSpecialTickets();
	menu.type_of_action = `addition_${type}`;
	console.info('addition step', type);
}

