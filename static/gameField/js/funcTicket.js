function onDragStartTicket(event) {
    this.data = event.data;
    this.dragging = true;
}

function onDragEndTicket() {//Если билет выбран, отключает выделение, иначе выделяет
    if (this.alpha == 1) {
        this.alpha = 0.5;
	document.canvasTickets.dragTickets.push(this.number + '+' + this.type);
        // plusCountArt(this.type.split('_')[1]);
    }
    else if(this.alpha == 0.5) {
        this.alpha = 1;
	document.canvasTickets.dragTickets.splice(document.canvasTickets.dragTickets.indexOf(this.number + '+' + this.type), 1);
        // minusCountArt(this.type.split('_')[1]);
    }
    console.log('drag tickets', document.canvasTickets.dragTickets);
    
    this.dragging = false;
    this.data = null;
}
