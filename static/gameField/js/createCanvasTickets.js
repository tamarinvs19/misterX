function createCanvasTickets(){ // создание canvasTickets
	scale = getTicketsScale();
	size = standartWidths['tickets'] * scale;

	var canvasTickets = new Vue({
		el: "#ticketsUL",
		data: {
			playerTickets : [],
			dragTickets: [],
			ticketWidth: size,
			ticketHeight: size * 2,
		},
		methods: {
			clickTicket: onclickTicket,
		}
	})
	if (reverse == false) {
		$('#divTickets')[0].style['height'] = (size + 5) * getPlayerTickets().length + 10 + 'px';
	}

	canvasTickets.playerTickets.splice(getPlayerTickets().length);

	document.canvasTickets = canvasTickets;
}

function updateTickets() {
	scale = getTicketsScale();
	size = standartWidths['tickets'] * scale;
	canvasTickets = document.canvasTickets;

	newTickets = getPlayerTickets();
	for (var i=0; i<newTickets.length; i++) {
		if (newTickets[i] != '') {
			newTickets[i] =  {
				name: i + '+' + newTickets[i],
				src: '/static/img/tickets/'+newTickets[i]+'.png',
				alt: newTickets[i],
			}
		} else {
			newTickets[i] =  {
				name: false,
				src: false,
				alt: '',
			}
		}

	}
	canvasTickets.playerTickets = newTickets;
}

function onclickTicket(event) {
	ticket = event.target;
	if (this.dragTickets.indexOf(ticket.id) != -1) {
		ticket.style['outline'] = 'none';
		ticket.style['opacity'] = "100%";
		this.dragTickets = this.dragTickets.filter(t => ticket.id != t);
	}
	else {
		ticket.style['outline'] = "4px solid #55af00";
		ticket.style['opacity'] = "60%";
		this.dragTickets.push(ticket.id);
	}
	console.log(this.dragTickets)
}
