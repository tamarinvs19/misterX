function createCanvasSpecialTickets() {
	scale = getTicketsScale();
	size = standartWidths['tickets'] * scale;

	let canvasSpecialTickets = new Vue({
		el: "#specialTicketsUL",
		data: {
			specialTickets : [],
			ticketWidth: size,
			ticketHeight: size * 2,
		},
	})

	menu.canvasSpecialTickets = canvasSpecialTickets;
}

function updateSpecialTickets() {
	if (menu.mode == 'collect') {
		scale = getTicketsScale();
		size = standartWidths['tickets'] * scale;

		newTickets = getSpecialTickets();
		console.log("New special tickets ", newTickets);
		if (newTickets.length != 0 && newTickets[0] == "") {
			newTickets = [];
		}
		for (let i=0; i<newTickets.length; i++) {
			newTickets[i] =  {
				name: i + '+' + newTickets[i],
				src: '/static/img/tickets/'+newTickets[i]+'.png',
				alt: newTickets[i],
			}
		}
		menu.canvasSpecialTickets.specialTickets = newTickets;
		if (newTickets.length != 0) {
			$('#specialMenu').css('width', (size + 5) * newTickets.length)
			$('#specialMenu').css('height', (2*size) * 2)
		}
		
		tickets = getPlayerTickets();
		updateTickets(tickets);
	}
}

