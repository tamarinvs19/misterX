let Application = PIXI.Application,
	resources = PIXI.loader.resources,
	loader = PIXI.loader,
	Sprite = PIXI.Sprite,
	Text = PIXI.Text,
	TextStyle = PIXI.TextStyle;

var nameTextureDetectives = "/static/gameField/texture/textureDetectivesJSON.json",
	nameTextureTickets = "/static/gameField/texture/textureTicketsJSON.json",
	nameTextureActs = "/static/gameField/texture/textureActsJSON.json",
	nameImgMap = '/static/gameField/map/mapL.png';

var standartWidths = {'ticketMX': 25, 'chat': 25, 'map': 1532, 'tickets': 40, 'players': 227},
	standartHeights = {'ticketMX': 50, 'chat': 50, 'map': 1165, 'tickets': 80, 'players': 227},
	mapFieldSize = {'x': document.getElementById('fieldOfMap').clientWidth, 'y': document.getElementById('fieldOfMap').clientWidth / standartWidths['map'] * standartHeights['map']},
	stepsFieldSize = getStepsFieldSize(),
	additionalFieldSize = {'x': document.getElementById("additionalTabs").clientWidth, 'y': mapFieldSize['y'] - stepsFieldSize['y']},
	ticketsFieldSize = {'x': document.getElementById("ticketsOfPlayer").clientWidth, 'y': document.getElementById("ticketsOfPlayer").clientWidth / standartWidths['tickets'] * standartHeights['tickets']},
	actsFieldSize = ticketsFieldSize;

var ticketScale = Math.trunc((stepsFieldSize['x'] - 30) / 6)/standartWidths['ticketMX'],
	mapScale = mapFieldSize['x'] / standartWidths['map'],
	playerScale = mapScale * 50 / standartWidths['players'],
	playerCoordScale = standartWidths['map'] / 7657 * mapScale;
var reverse = false;
var menu;
var mode = 'normal';


checkReverse();
getRole();
getQueue();
getColorMisterX();
getColors();

function getRole() {
	getPlayerRole();
	if (document.roleGamer == 'misterx') {
		console.log(document.roleGamer);
		menu = new MisterXSpecialMenu();
	}
	else
		menu = new DetectivesSpecialMenu();
}

function getStepsFieldSize() {
	width = $('#stepsOfMisterx')[0].clientWidth
	if (document.body.clientWidth < 550) {
		return {'x': width, 'y': 1.85 * width}
	}
	else if (width > 350) {
		return {'x': width, 'y': width / 2.65}
	}
	else {
		return {'x': width, 'y': mapFieldSize['y'] / 1.85}
	}
}

function checkReverse() {
	div = document.getElementById("stepsOfMisterx");
	if (div.offsetWidth > 550) { reverse = true }
	else { reverse = false }
}

function getMXTicketsScale() {
	div = document.getElementById("stepsOfMisterx");
	width = div.offsetWidth;
	if (reverse == false) {
		return ticketScale;
	}
	else {
		return ((width - 100) / 8) / standartWidths['ticketMX'] * 0.75;
	}
}

function getTicketsScale() {
	div = document.getElementById("ticketsOfPlayer");
	width = div.offsetWidth;
	console.info('Width:   ', width);
	if (reverse == false) {
		return (width - 10) / standartHeights['tickets'];
	}
	else {
		return ((width - 100 - 100) / 9) / standartWidths['tickets'];
	}
}
