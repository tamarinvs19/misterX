function createEvidences(evidences) {
    let field = document.getElementById("evidencesField");
    // console.log('createEvi===',evidences, field.evidences)
    for (var i=0; i<evidences.length; i++) {
        if (field.evidences.length > i && field.evidences[i] != evidences[i]) {
            if (field.evidencesTR[i] != undefined) {
                field.evidencesTR[i].remove();
            }
            addEvidence(evidences[i], i);
        }
        else if (field.evidences.length <= i) {
            addEvidence(evidences[i], i);
        }
    }
}

function addEvidence(evidence, number) {
    let field = document.getElementById("evidencesField");
    let table = document.getElementById("evidencesTable");
    var newTR = document.createElement('tr');
    evidence = evidence.split('+');
    newTR.innerHTML = '<td>' + (parseInt(evidence[0]) + 1) + '</td><td>' + evidence[1] + '</td>';

    // console.log(field.evidence, evidence, number)
    if (field.evidences.length >= number) {
        field.evidences[number] = evidence;
        field.evidencesTR[number] = newTR;
    }
    else {
        field.evidence[number] = evidence;
        field.evidencesTR[number] = newTR;
    }
    table.appendChild(newTR);
}

