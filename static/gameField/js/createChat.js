function createCanvasChat(){
	var canvasChat = new Vue({
		delimiters: ['[[', ']]'],
		el: "#messagesUL",
		data: { 
			chat: [
				{time: '', author: '', text: '', class: ''}
			],
		}
	})

	document.canvasChat = canvasChat;
	getChat();
}

