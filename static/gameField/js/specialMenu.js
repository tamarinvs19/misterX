class SpecialMenu {
	constructor() {
		this.mode = "choice";

		this.div = $("#specialMenu")[0];
		this.menu = $("#mainSpecialMenu")[0];

		this.div.onmousedown = saveXY;
		this.div.onmouseup = clearXY;
		this.div.onmousemove = null;
		document.deltaX_ = 0;
		document.deltaY_ = 0;

		this.specialTickets = [];
	}

	openButtons() {}
	closeButtons() {}

	clearSpecialTickets() {
		document.canvasTickets.dragTickets.forEach((item, i, arr) => document.getElementById(arr[i]).style['outline'] = 'none');
		document.canvasTickets.dragTickets.forEach((item, i, arr) => document.getElementById(arr[i]).style['opacity'] = '100%');
		document.canvasTickets.dragTickets = [];

		if (document.doubleTickets) {
			document.doubleTickets.forEach((item, i, arr) => document.getElementById(arr[i]).style['outline'] = 'none');
			document.doubleTickets.forEach((item, i, arr) => document.getElementById(arr[i]).style['opacity'] = '100%');
			document.doubleTickets = [];
		}
		if (document.doubleStepType != undefined)
			document.doubleStepType = "";
		if (document.doublePath != undefined)
			document.doublePath = [];
		if (document.doubleStep != undefined)
			document.doubleStep = false;
		if (document.doubleStepCount != undefined)
			document.doubleStepCount = 0;

		this.specialTickets.forEach((item, i, arr) => document.getElementById(arr[i]).style['outline'] = 'none');
		this.specialTickets.forEach((item, i, arr) => document.getElementById(arr[i]).style['opacity'] = '100%');
		updateTickets(getPlayerTickets());
		this.specialTickets = [];

		let app = document.canvasMap;
		positions = getPositions();
		for (let i=0; i<app.players.length; i+=1) {
			let player = app.players[i];
			if (player.type == 'white') {
				let pos = getPositionByNumber(positions[player.type]);
				player.position.set(pos[0], pos[1]);
				player.numCity = positions[player.type];
			}
		}
		this.menu.innerHTML = "";
	}

	open() {
		if (document.roleGamer == 'misterx' || !getPossibleSteps()) {
			$(this.div).css("visibility", "visible");
			if (this.mode == "choice") {
				this.openButtons();
			}
		}
	}

	close() {
		$(this.div).css("visibility", "hidden");
		this.clearSpecialTickets();
		this.mode = "choice";
		appSpecialActs.text = null;
	}
}

class MisterXSpecialMenu extends SpecialMenu {
	constructor() {
		super();
	}

	openButtons() {
		let selectedTickets = document.canvasTickets.dragTickets;
		let countSecret = selectedTickets.reduce(
			(sum, ticket) => (ticket.split('_')[1] == 'secret' ? 1 : 0) + sum,
			0);
		let countDouble = selectedTickets.reduce(
			(sum, ticket) => (ticket.split('_')[1] == 'double' ? 1 : 0) + sum,
			0);

		let content = "";
		console.log(selectedTickets, countSecret, countDouble);
		if (selectedTickets.length == 3) {
			if (countSecret == 3)
				content += "<img class='menuMXTickets' src='/static/img/special_acts/secret.png' onclick='secretStep()'>";
			if (countDouble == 3)
				content += "<img class='menuMXTickets' src='/static/img/special_acts/double.png' onclick='standartDoubleStep()'>";
		} else if (selectedTickets.length == 6) {
			if (countSecret == 3 && countDouble == 3) {
				content += "<img class='menuMXTickets2' src='/static/img/special_acts/secret_double.png' onclick='secretDoubleStep()'>";
				content += "<img class='menuMXTickets2' src='/static/img/special_acts/double_secret.png' onclick='doubleSecretStep()'>";
			}
		}

		this.mode = "choice";
		this.menu.innerHTML = content;
	}
}

class DetectivesSpecialMenu extends SpecialMenu {
	constructor() {
		super();
		this.type_of_action = "";
	}

	openButtons() {
		// add test queue
		let content = "";
		if (document.canvasMisterXTickets.steps.length >= 5) {
			content += `<img class='menuDetectivesTickets' src='/static/img/special_acts/search_4.png' onclick='searchStep(4)'>`;
			content += `<img class='menuDetectivesTickets' src='/static/img/special_acts/search_3.png' onclick='searchStep(3)'>`;
			content += `<img class='menuDetectivesTickets' src='/static/img/special_acts/search_2.png' onclick='searchStep(2)'>`;

			content += `<img class='menuDetectivesTickets' src='/static/img/special_acts/addition_car.png' onclick='useAdditionStep("car")'>`;
			content += `<img class='menuDetectivesTickets' src='/static/img/special_acts/addition_train.png' onclick='useAdditionStep("train")'>`;
			content += `<img class='menuDetectivesTickets' src='/static/img/special_acts/addition_plane.png' onclick='useAdditionStep("plane")'>`;
			$('#specialMenu').css('height', '45%');
		}

		this.mode = "choice";
		this.menu.innerHTML = content;
	}

	closeButtons() {
		this.menu.innerHTML = '';
	}

	openSpecialTickets() {
		this.menu.innerHTML =
			`
			 <input type='button' class='userButtons' value='Добавить билеты' onclick='menu.addSpecialTickets()'>
			 <div id="divSpecialTickets">
				<ul id="specialTicketsUL" class="ticketsField">
				  <li type='none' class='ticketsLI'
							v-for="(ticket, index) in specialTickets"
							v-bind:id="'ticket' + index">
					<img v-if="ticket.name"
						:height='ticketHeight'
						:width='ticketWidth'
						:id="ticket.name"
						:src='ticket.src'
						:alt="ticket.name"
						>
				  </li>
				</ul>
			  </div>
			 <input type='button' class='userButtons' value='Использовать' onclick='menu.useSpecialTickets()'>
			  `;
		createCanvasSpecialTickets();
		this.mode = 'collect';
	}

	clearSpecialTickets() {
		clearTicketsForSpecialAct();
		this.type_of_action = "";
		SpecialMenu.prototype.clearSpecialTickets.call(this);
	}

	addSpecialTickets() {
		if (this.mode == 'collect' && document.canvasTickets.dragTickets.length > 0) {
			let res = addTicketsForSpecialAct();
			if (res) {
				updateSpecialTickets();
			}
			document.canvasTickets.dragTickets = [];
		}
	}

	useSpecialTickets() {
		if (this.type_of_action.indexOf('evidence') != -1) {
			if (checkEvidenceStep()) {
				menu.close();
			}
		} else if (this.type_of_action.indexOf('addition') != -1) {
			menu.mode = 'addition moving';
			$(menu.div).css("visibility", "hidden");
			let type = this.type_of_action.split('_')[1];
			appSpecialActs.text = `Переместите одну из ваших фигурок на ${type}`;
		}
	}
}

function saveXY(event) {
	document.deltaX_ = event.clientX - this.offsetLeft;
	document.deltaY_ = event.clientY - this.offsetTop;
	this.onmousemove = move;
}

function clearXY() {
	this.onmousemove = null;
}

function move(event) {
	new_x = - document.deltaX_ + event.clientX;
	new_y = - document.deltaY_ + event.clientY;
	$(this).css('left', new_x + 'px');
	$(this).css('top', new_y + 'px');
}

