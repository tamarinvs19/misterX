function addMisterXTickets(number, type) {
    let app = document.canvasMisterXTickets;
    var nameTicket = type + '.png';
    let ticket = new Sprite(app.texture[nameTicket]);

    //Настройка билета
    scale = getMXTicketsScale();
    size = standartWidths['ticketMX'] * scale;

    // number -= 1
    ticket.type = type;
    app.steps[number] = ticket;

    ticket.anchor.set(1, 0);
    x = (Math.trunc(number/8)*2*(size+5))+5;
    y = (number % 8) * 4/3*(size+5)+20*scale;
    
    if (reverse) {
	ticket.position.set(y+size, x);
	ticket.width = size;
	ticket.height = 2*size;
    } else {
	ticket.rotation = -3.14/2;
	ticket.position.set(x, y);
	ticket.width = size;
	ticket.height = 2*size;
    }
    app.stage.addChild(ticket);
    console.log('add mx ticket', number);
}

