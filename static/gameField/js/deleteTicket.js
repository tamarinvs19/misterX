function deleteCheckingTickets(){//Удаляет выбранные билеты
	if (!getPossibleSteps()) { 
		let app = document.canvasTickets;
		new_tickets = removeTickets(app.dragTickets);
		console.log(new_tickets);
		
		updateTickets();
		clearDragTickets();
	}
}

function clearDragTickets() {
    document.canvasTickets.dragTickets = [];
}

function delTicket(name){//удаляет билет по имени
    let app = document.canvasTickets;
    tickets = document.playerTickets.map(item => item.name)

    console.log(app.dragTickets.indexOf(name), name)

    if (app.dragTickets.indexOf(name) != -1) {
        app.dragTickets.splice(app.dragTickets.indexOf(name), 1);
	app.playerTickets.splice(tickets.indexOf(name), 1);
    }
}

// This is the end of the course
function remakeTickets() {
	if (!getPossibleSteps()) { 
		tickets = compliteCourse();
		updateTickets(tickets);
	}
}
