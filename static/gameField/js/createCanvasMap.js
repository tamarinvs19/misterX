function createCanvasMap() {
	let app = new Application({
		width: mapFieldSize['x'],
		height: mapFieldSize['y'],
		antialias: true,
		transparent: false,
		resolution: 1
	}
	);
	document.getElementById('fieldOfMap').appendChild(app.view);

	app.renderer.backgroundColor = 0x777777;
	document.canvasMap = app;

	let map = new Sprite(resources[nameImgMap].texture);
	map.interactive = true;
	map
		.on('touchstart', showCityInfoOpen)
		.on('touchend', showCityInfoClose)
		.on('touchendoutside', showCityInfoClose)
		.on('mousedown', showCityInfoOpen) 
		.on('mouseupoutside', showCityInfoClose)
		.on('mouseup', showCityInfoClose);

	map.width = mapFieldSize['x']; //7657
	map.height = mapFieldSize['y'];//5823
	app.stage.addChild(map);
	createPlayers();
}


