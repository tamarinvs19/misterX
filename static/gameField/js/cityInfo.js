function showCityInfoOpen(event) {
	let app = document.canvasMap;
	const textStyle = new TextStyle({
		fontFamily: "Arial",
		fontSize: 24,
		fill: "#af0000",
	});
	let position = event.data.getLocalPosition(this.parent);
	position = attachmentToCity(position);

	const cityText = new Text(position['num'], textStyle);
	cityText.anchor.set(0.5);
	cityText.position.set(position['x'], position['y']);

	const txtBG = new PIXI.Sprite(PIXI.Texture.WHITE);
	txtBG.width = cityText.width + 5, txtBG.height = cityText.height + 5;
	txtBG.anchor.set(0.5);
	txtBG.position.set(position['x'], position['y']);

	const cage = new PIXI.Container();
	cage.addChild(txtBG, cityText);

	cage.name = "cityText";
	cage.cityText = cityText;
	cage.txtBG = txtBG;

	app.stage.addChild(cage);
	this.cage = cage;
}

function showCityInfoClose() {
	let app = document.canvasMap;
	app.stage.removeChild(this.cage);
}
