function resizeFields(){
	heightTitle = 24;
	document.getElementById('fieldOfMap').style.height = mapFieldSize['y'] + heightTitle + 'px';
	if (reverse == false) {
		document.getElementById('stepsOfMisterx').style.height = stepsFieldSize['y'] + heightTitle + 'px';
	} else {
		height = (stepsFieldSize['x'] - 40) / 8 * 6 * 0.75 ;
		$('#stepsOfMisterx')[0].style['height'] = height + 25 + 'px';
	}
	document.getElementById('extremalAdditionalDiv').style.height = additionalFieldSize['y']+ heightTitle/2 + 'px';
	xs = document.getElementsByClassName('additionalField');
	for (i=0; i<xs.length; i++){
		xs[i].style.height = additionalFieldSize['y']-2*heightTitle + 'px';
	}
	document.getElementById('evidencesTable').style.width = additionalFieldSize['x'] -25 + 'px' ;

	if (reverse == false) {
		// document.getElementById('ticketsOfPlayer').style.height = "auto";
		document.getElementById('specialActs').style.height = "auto";
		xs = document.getElementsByClassName('userButtons');
		for (i=0; i<xs.length; i++){
			xs[i].style.height = 40 + 'px';
			xs[i].style.width = "98%";
		}
		uls = $('ul.ticketsField');
		uls.css('transform', 'rotate(-90deg)');
	} else {
		// document.getElementById('ticketsOfPlayer').style.height = 2*(standartWidths['tickets'] * getTicketsScale() + 5) + heightTitle + 'px';
		document.getElementById('specialActs').style.height = 2*(standartWidths['tickets'] * getTicketsScale() + 5) + heightTitle + 'px';
		uls = $('ul.ticketsField');
		uls.css('transform', 'none');
		xs = document.getElementsByClassName('userButtons');
		for (i=0; i<xs.length; i++){
			xs[i].style.height = (standartWidths['tickets'] * getTicketsScale() - 1)  + 'px';
		}
	}
	// xs = document.getElementsByClassName('moveButtons');
	// for (i=0; i<xs.length; i++){
	// xs[i].style.height = (standartWidths['tickets'] * getTicketsScale() - 1)  + 'px';
	// }
}
