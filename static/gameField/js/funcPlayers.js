function onDragStartPlayer(event) {//при нажатии мышкой на изображение детектива
	this.data = event.data;
	this.alpha -= 0.3;
	this.dragging = true;
	this.click = true;
}

function onDragEndPlayer() {
	//привязка к городу
	if (this.click == true){
		let playerPos = this.data.getLocalPosition(this.parent);
		let playerCity = this.numCity;
		let pos = attachmentToCity(playerPos);

		console.info("Player's position ?", pos['num'], cities[pos['num']-1]);

		if (menu.mode == 'secret') {
			let testServer = checkSecretStep(pos['num']);
			console.info("Test to secret step of gamer",
				'test -', testServer);

			let num = getPositions()['white'];
			this.position.set(cities[num-1]['x'] * playerCoordScale, cities[num-1]['y'] * playerCoordScale);
			this.numCity = num;

			if (testServer == true) {
				menu.close();
				updateTickets(getPlayerTickets());
			}
		} else if (document.doubleStep == true) {
			let testPath = testPathCity(pos['num'], playerCity);
			let tickets = document.canvasTickets.dragTickets;
			console.log('doubleStep', tickets, testPath['type'])

			if (testPath['test'] == true && tickets.length == 1 &&
				testPath['type'].indexOf(tickets[0].split('+')[1].split('_')[0]) != -1 
				) {
				addDoubleStep(tickets[0], pos['num']);
				this.position.set(pos['x'], pos['y']);
				this.numCity = pos['num'];
				document.canvasTickets.dragTickets.forEach((item, i, arr) => document.getElementById(arr[i]).style['outline-color'] = '#af0000');
				clearDragTickets();
			}

			if (document.doubleStepCount == 2) {
				var testServer = checkDoubleStep();
				console.info("Test to move gamer",
					'test -', testServer);

				let num = getPositions()['white'];
				console.log(num);
				this.position.set(cities[num-1]['x'] * playerCoordScale, cities[num-1]['y'] * playerCoordScale);
				this.numCity = num;
				if (testServer) {
					updateTickets(getPlayerTickets());
					menu.close();
				}
			}
		} else if (menu.mode == 'addition moving') {
			let testServer = checkAdditionStep(pos['num'], this.type)
			console.info("Test to move gamer",
				'test -', testServer);

			let num = getPositions()[this.type];
			this.position.set(cities[num-1]['x'] * playerCoordScale, cities[num-1]['y'] * playerCoordScale);
			this.numCity = num;
			if (testServer) {
				updateTickets(getPlayerTickets());
				menu.close();
			}
		} else {
			let testServer = checkMovePlayer([playerCity, pos['num']],
				document.canvasTickets.dragTickets, this.type, document.specialAct); //from gameField.html
			console.info("Test to move gamer",
				'test -', testServer);

			let num = testServer['res_city'];
			this.position.set(cities[num-1]['x'] * playerCoordScale, cities[num-1]['y'] * playerCoordScale);
			this.numCity = num;
			updateTickets(testServer['res_tickets'].split(','));

			document.specialAct = [];
			if (testServer['result'] == true) {
				clearDragTickets();
			}
		}

		this.alpha += 0.3;
		this.dragging = false;
		this.click = false;
		this.data = null;
	}
}

function onDragMovePlayer() {//во время движения мышки
	if (this.dragging) {
		var newPosition = this.data.getLocalPosition(this.parent);
		this.x = newPosition.x;
		this.y = newPosition.y;
	}
}

