loader
    .add([nameTextureDetectives, nameTextureTickets, nameTextureActs, nameImgMap])
    .on("progress", loadProgressHandler)
    .load(setup);


function loadProgressHandler() {
    console.info("loading");
}
function setup(){
    console.info("MAIN SETUP;", ' ROLE', document.roleGamer);
    createCanvasMap();
    createCanvasMisterXTickets();
    createCanvasChat();
    createCanvasTickets();
    updateTickets();

    document.doubleStep = false;
    document.additionStep = false;

    resizeFields();
    updateMisterXTickets();
    document.getElementById('evidencesField').evidences = [];
    document.getElementById('evidencesField').evidencesTR = [];
    updateEvidences();

    poll();
}
    
