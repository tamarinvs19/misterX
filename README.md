# Mister X
Реализация настольной игры "Мистер Х" в формате игры для нескольких человек на веб-сайте. Адрес: https://misterx.herokuapp.com

## Описание игры
Один из игроков становиться Мистером X и должен добраться от выбранного им города до одного из нескольких городов того же цвета.
Оставшиеся детективы должны помешать ему.

Детективы знают из городов какого цвета отправился Мистер Х, на каком транспорте он передвигается, а также узнают, что он недавно был в каком-то городе, если заканчивают в нем свой ход.

Передвижения по карте осуществляются с помощью билетов, кроме этого существуют специальные действия, для применения которых требуется сбросить несколько билетов заданного типа.

Если один из детективов оказался в одном городе с Мистером Х, побеждает команда детективов, а если Мистер Х добрался в один из городов нужного цвета, то он побеждает.

Полные правила игры можно посмотреть [здесь](https://misterx.herokuapp.com/rules/). Настольная игра есть [на сайте GaGa](https://gaga.ru/game/misterx/). На оригинальном сайте нашел только [правила](https://www.ravensburger.us/spielanleitungen/ecm/Spielanleitungen/Mister%20X.pdf?ossl=pds_text_Spielanleitung).

О том, как совершать специальные действия, можно прочитать [здесь](https://misterx.herokuapp.com/specialActs/).

## Используемые технологии
* Серверная часть написана на Python3, с использованием фреймворка Django.
* Облачная платформа Heroku используется для развертывания приложения. 
* Интерфейс: HTML страница. Основной функционал реализован с помощью JavaScript, в том числе библиотек PixiJS, jQuery и Vue.js.

## Реализация на данный момент
Полноценная игра для 2-6 человек, все правила соблюдаются. Есть собственная система аутентификации с django-allauth, в том числе вход через аккаунты других соцсетей. Поддерживается управление с сенсорного экрана.

### Планы:
* Адаптация под маленький экран
* Django channels
* Подписывание запросов

